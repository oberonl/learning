package com.gustoglad.springtestproject;

import com.gustoglad.springtestproject.retry.RetryTest;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
class SpringTestProjectApplicationTests {

    @Autowired
    private RetryTest retryTest;

    @Autowired
    private AbstractApplicationContext context;

    @Test
    void contextLoads() {
    }

    @Test
    public void retryTest() {
        retryTest.testRetry();
    }

    @Test
    public void lifecycleTest() {
        context.getBean("lifecycleTest");
    }
}
