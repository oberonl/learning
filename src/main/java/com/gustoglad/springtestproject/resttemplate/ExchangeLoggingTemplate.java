package com.gustoglad.springtestproject.resttemplate;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.client.*;
import org.springframework.web.client.RestTemplate;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.stream.Collectors;

@Slf4j
@Configuration
public class ExchangeLoggingTemplate {

    @Bean
    public RestTemplate createDebuggingRestTemplate() {
        RestTemplate restTemplate = new RestTemplate(getClientHttpRequestFactory());
        restTemplate.setInterceptors(Collections.singletonList(new CustomClientHttpRequestInterceptor()));
        return restTemplate;
    }

    private class CustomClientHttpRequestInterceptor implements ClientHttpRequestInterceptor {

        @Override
        public ClientHttpResponse intercept(
                HttpRequest request, byte[] body,
                ClientHttpRequestExecution execution) throws IOException {

            logRequestDetails(request);
            ClientHttpResponse execute = execution.execute(request, body);
            logResponseDetails(execute);
            return execute;
        }


        private void logRequestDetails(HttpRequest request) {
            log.info("Headers: {}", request.getHeaders());
            log.info("Request Method: {}", request.getMethod());
            log.info("Request URI: {}", request.getURI());
        }

        private void logResponseDetails(ClientHttpResponse response) {
            String body = extractResponseBody(response);
            HttpStatus statusCode = extractStatusCode(response);
            log.info("Headers: {}", response.getHeaders());
            log.info("Response body: {}", body);
            log.info("Response status: {}", statusCode);
        }

        private HttpStatus extractStatusCode(ClientHttpResponse response) {
            HttpStatus statusCode = null;
            try {
                statusCode = response.getStatusCode();
            } catch (IOException e) {
                log.error("Ошибка чтения статуса ответа HTTP запроса");
            }
            return statusCode;
        }

        private String extractResponseBody(ClientHttpResponse response) {
            String errorText = "Error Reading Response Body";

            try (InputStreamReader isr = new InputStreamReader(response.getBody(), StandardCharsets.UTF_8)) {
                return new BufferedReader(isr)
                        .lines()
                        .collect(Collectors.joining("\n"));

            } catch (IOException e) {
                log.error("Ошибка чтения тела ответа HTTP запроса", e);
                return errorText;
            }
        }
    }

    private ClientHttpRequestFactory getClientHttpRequestFactory() {
        SimpleClientHttpRequestFactory factory = new SimpleClientHttpRequestFactory();
        factory.setConnectTimeout(3_000);
        return new BufferingClientHttpRequestFactory(factory);
    }
}
