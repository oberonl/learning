package com.gustoglad.springtestproject.retry;

import lombok.extern.slf4j.Slf4j;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Recover;
import org.springframework.retry.annotation.Retryable;
import org.springframework.stereotype.Component;

import java.util.concurrent.atomic.AtomicInteger;

@Slf4j
@Component
/**
 * Методы @Retryable и @Recover должны быть public, сигнатура методов должна совпадать, с точностью до бросаемого исключения.
 */
public class RetryTest {

    private AtomicInteger counter = new AtomicInteger(0);

    @Retryable(
            value = {NullPointerException.class},
            backoff = @Backoff(delay = 1000)
    )
    public String testRetry() {
        counter.incrementAndGet();
        log.info("counter is {}", counter);
        throw new NullPointerException();
    }

    @Recover
    public String recover(Exception e) {
        log.info("counter on recovery is {}", counter);
        log.error("", e);
        return "";
    }

}