package com.gustoglad.springtestproject.lifecycle;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Slf4j
@Component
@Scope("prototype")
public class LifecycleTest implements InitializingBean {

    public LifecycleTest() {
        log.info("Constructor");
    }

    @Override
    public void afterPropertiesSet() {
        log.info("InitializingBean");
    }

    @PostConstruct
    public void postConstruct() {
        log.info("PostConstruct");
    }

    // не вызывается
    public void init() {
        log.info("init-method");
    }
}

