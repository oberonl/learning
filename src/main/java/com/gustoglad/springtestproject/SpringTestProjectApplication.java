package com.gustoglad.springtestproject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.retry.annotation.EnableRetry;

@EnableRetry
@SpringBootApplication
public class SpringTestProjectApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringTestProjectApplication.class, args);
	}

}
